import "./App.css";
import React, { useState } from "react";

import Title from "./components/molecule/Title";
import TodoList from "./components/organism/TodoList";
import PopUp from "./components/molecule/PopUp";
import PopUpAddButton from "./components/organism/PopUpAddButton";
import Overlay from "./components/atom/Overlay";

function App() {
  const [newTaskTitle, setNewTaskTitle] = useState("");
  const [newTaskDate, setNewTaskDate] = useState("");
  const [showPopUp, setShowPopUp] = useState(false);

  const handleTogglePopUp = () => {
    setShowPopUp((prevState) => !prevState);
  };

  const handleClosePopUp = () => {
    setShowPopUp(false);
  };

  const handleResetForm = () => {
    setNewTaskTitle("");
    setNewTaskDate("");
  };

  return (
    <div className="App">
      <Title />
      <TodoList />
      <PopUpAddButton
        showPopUp={showPopUp}
        handleTogglePopUp={handleTogglePopUp}
        handleClosePopUp={handleClosePopUp}
      />
      <Overlay show={showPopUp} onClick={handleClosePopUp} />
      {showPopUp && (
        <PopUp
          onClose={handleClosePopUp}
          taskTitle={newTaskTitle}
          handleAddTaskTitle={setNewTaskTitle}
          taskDate={newTaskDate}
          handleAddTaskDate={setNewTaskDate}
          handleResetForm={handleResetForm}
        />
      )}
    </div>
  );
}

export default App;
