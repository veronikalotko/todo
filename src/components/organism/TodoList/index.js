import React, { useState, useEffect } from "react";
import styled from "@emotion/styled";
import Todo from "../../molecule/ToDo";
import PopUp from "../PopUpAddButton";
import axios from "axios";

const TodoListContainer = styled.div`
  max-width: 768px;
  width: 100%;
  height: 550px;
`;

const TodoList = () => {
  const [list, setList] = useState([]);
  console.log(list);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          "https://l3cda8qoig.execute-api.eu-central-1.amazonaws.com/main/todo-point"
        );
        setList(response.data.Items);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);

  const handleCheck = async (element) => {
    // event.preventDefault();
    try {
      console.log("hello");
      const response = await axios.put(
        "https://l3cda8qoig.execute-api.eu-central-1.amazonaws.com/main/todo-point",
        element
      );
      console.log(response);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <TodoListContainer>
      {list.length > 0 &&
        list.map((element, index) => (
          <Todo
            key={index}
            element={element}
            index={index}
            title={element.title}
            date={element.date}
            isChecked={element.isChecked}
            onCheck={handleCheck}
            list={list}
            setList={setList}
          />
        ))}
      <PopUp />
    </TodoListContainer>
  );
};

export default TodoList;
