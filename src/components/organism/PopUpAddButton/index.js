import React from "react";
import styled from "@emotion/styled";
import PopUp from "../../molecule/PopUp";
import Overlay from "../../atom/Overlay";

const Button = styled.div`
  width: 50px;
  height: 50px;
  margin: 20px;
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.2);
  background-color: #0277bd;
  border-radius: 24px;
  border: 0;
  margin: auto;
  z-index: 2;
  transition: 0.3s;
  svg {
    margin: 15px;
  }
  cursor: pointer;
  position: fixed;
  bottom: 40px;
  left: 0px;
  right: 0px;
  &.active {
    transform: rotate(45deg);
  }
`;

const PopUpAddButton = ({ showPopUp, handleTogglePopUp, handleClosePopUp }) => {
  return (
    <>
      <Button className={showPopUp ? "active" : ""} onClick={handleTogglePopUp}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
          width="18"
          height="18"
        >
          <defs>
            <path id="a" d="M0 0h24v24H0z" />
            <path id="b" d="M0 0h24v24H0z" />
          </defs>
          <g fill="none" fillRule="evenodd" transform="translate(-3 -3)">
            <mask id="c" fill="#fff">
              <use xlinkHref="#b" />
            </mask>
            <path
              fill="#FFF"
              d="M20.25 12c0 .4-.35.75-.75.75h-6.75v6.75c0 .4-.35.75-.75.75s-.75-.35-.75-.75v-6.75H4.5c-.4 0-.75-.35-.75-.75s.35-.75.75-.75h6.75V4.5c0-.4.35-.75.75-.75s.75.35.75.75v6.75h6.75c.4 0 .75.35.75.75Z"
              mask="url(#c)"
            />
          </g>
        </svg>
      </Button>
      <Overlay show={showPopUp} onClick={handleClosePopUp} />
      {showPopUp && <PopUp />}
    </>
  );
};

export default PopUpAddButton;
