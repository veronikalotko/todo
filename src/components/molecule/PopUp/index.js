import React from "react";
import styled from "@emotion/styled";
import TaskInputDate from "../../atom/TaskInputDate";
import TaskInputTitle from "../../atom/TaskInputTitle";
import SubmitButton from "../../atom/SubmitButton";
import axios from "axios";

const PopUpContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: white;
  padding: 20px 20px;
  width: 80%;
  max-width: 380px;
  border-radius: 20px;
`;

const PopUpBackground = styled.div`
  background-color: #ffffff;
  border-radius: 4px;
  margin: auto;
  pointer-events: auto;
`;

const PopUp = ({
  handleResetForm,
  onClose,
  taskTitle,
  handleAddTaskTitle,
  taskDate,
  handleAddTaskDate,
}) => {
  const handleSubmit = async (event) => {
    event.preventDefault();

    if (taskTitle && taskDate) {
      try {
        const response = await axios.post(
          "https://l3cda8qoig.execute-api.eu-central-1.amazonaws.com/main/todo-point",
          {
            title: taskTitle,
            date: taskDate,
            isChecked: false,
          }
        );
        console.log(response.data);
        handleResetForm();
        onClose();
      } catch (error) {
        console.error(error);
      }
    }
  };

  return (
    <PopUpContainer>
      <form onSubmit={handleSubmit}>
        <PopUpBackground>
          <TaskInputTitle value={taskTitle} handleChange={handleAddTaskTitle} />
          <TaskInputDate value={taskDate} handleChange={handleAddTaskDate} />
          <SubmitButton type="submit" onClick={onClose} />
        </PopUpBackground>
      </form>
    </PopUpContainer>
  );
};

export default PopUp;
