import React from "react";
import styled from "@emotion/styled";
import Greetings from "../../atom/Greetings";
import IncompleteTasksCount from "../../atom/IncompleteTasksCount";

const TitleContainer = styled.div`
  text-align: left;
  margin: 20px;
`;

const Title = () => {
  return (
    <TitleContainer>
      <Greetings />
      <IncompleteTasksCount />
    </TitleContainer>
  );
};
export default Title;
