import React from "react";
import styled from "@emotion/styled";
import TodoTitle from "../../atom/TodoTitle";
import TodoDate from "../../atom/TodoDate";
import TodoCheckbox from "../../atom/TodoCheckbox";
import { useState } from "react";
const TodoContainer = styled.div`
  background-color: #ffffff;
  margin: 10px;
  border-radius: 4px;
  align-items: center;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1);
`;

const TodoContent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  margin: 12px 10px;
  justify-content: left;
  gap: 10px;
  padding: 10px 5px;
`;

const TodoTitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const Todo = ({
  element,
  title,
  date,
  isChecked,
  onCheck,
  setList,
  index,
  list,
}) => {
  // const [isCheckedState, setIsCheckedState] = useState(isChecked);

  const handleCheckboxChange = () => {
    // setIsCheckedState((old) => {
    //   setIsCheckedState(!old);
    // });

    // setList((old) => {
    //   old[index] = {
    //     ...element,
    //     isChecked: !isChecked,
    //   };
    //   return old;
    // });
    onCheck(element);
    console.log(isChecked);
  };

  return (
    <TodoContainer>
      <TodoContent>
        <TodoCheckbox checked={isChecked} onChange={handleCheckboxChange} />
        <TodoTitleWrapper>
          <TodoTitle checked={isChecked} title={title} />
          <TodoDate date={date} />
        </TodoTitleWrapper>
      </TodoContent>
    </TodoContainer>
  );
};

export default Todo;
