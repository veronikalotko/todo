import React from "react";
import styled from "@emotion/styled";

const Task = styled.h2`
  font-size: 1rem;
  text-align: left;
  margin-left: 5px;
  font-weight: 500;
  margin: 0;
  text-decoration: ${(props) => (props.checked ? "line-through" : "none")};
  opacity: ${(props) => (props.checked ? "0.5" : "1")};
`;

const TodoTitle = ({ checked, title }) => {
  return <Task checked={checked}>{title}</Task>;
};

export default TodoTitle;
