import React from "react";
import styled from "@emotion/styled";

const HelloThereTitle = styled.h1`
  font-size: 1.85em;
  font-weight: 500;
`;

const Greetings = () => {
  return <HelloThereTitle>Hello there!</HelloThereTitle>;
};
export default Greetings;
