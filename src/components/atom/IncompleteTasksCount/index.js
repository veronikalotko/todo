import React from "react";
import styled from "@emotion/styled";

const TasksCount = styled.p``;

const IncompleteTasksCount = () => {
  return <TasksCount>You have 4 incomplete tasks</TasksCount>;
};
export default IncompleteTasksCount;
