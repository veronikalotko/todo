import React from "react";
import styled from "@emotion/styled";

const TitleWrapper = styled.div`
  text-align: left;
  width: 100%;
  max-width: 340px;
  margin: 15px auto;

  label {
    display: block;
    font-size: 0.8rem;
    font-weight: 500;
    margin: 5px 0;
  }
  input {
    width: 100%;
    height: 36px;
    border-radius: 4px;
    border: solid 1px #bdbdbd;
    &::placeholder {
      padding: 10px;
    }
  }
`;

const TaskInputTitle = ({ title, handleChange }) => {
  return (
    <TitleWrapper>
      <label htmlFor="title">Title</label>
      <input
        id="title"
        name="title"
        placeholder="What do you have to do?"
        value={title}
        onChange={(e) => handleChange(e.target.value)}
      />
    </TitleWrapper>
  );
};
export default TaskInputTitle;
