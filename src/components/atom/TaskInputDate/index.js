import React from "react";
import styled from "@emotion/styled";

const DateWrapper = styled.div`
  text-align: left;
  width: 100%;
  max-width: 340px;
  margin: 15px auto;

  label {
    display: block;
    font-size: 0.8rem;
    font-weight: 500;
    margin: 5px 0;
  }

  input {
    width: 100%;
    height: 36px;
    border-radius: 4px;
    border: solid 1px #bdbdbd;
    &::placeholder {
      padding: 10px;
    }
  }
`;

const TaskInputDate = ({ date, handleChange }) => {
  return (
    <DateWrapper>
      <label htmlFor="date">Date</label>
      <input
        type="date"
        placeholder="yyyy-mm-dd"
        value={date}
        onChange={(e) => handleChange(e.target.value)}
      />
    </DateWrapper>
  );
};

export default TaskInputDate;
