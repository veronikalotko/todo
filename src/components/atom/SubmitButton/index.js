import React from "react";
import styled from "@emotion/styled";

const Button = styled.button`
  width: 100%;
  max-width: 340px;
  height: 36px;
  padding-top: var(--telekom-spacing-unit-x6);
  font-size: 0.8rem;
  color: #ffffff;
  background-color: #0277bd;
  border-radius: 18px;
  border: 0;
  margin: 5px;
  cursor: pointer;
`;

const SubmitButton = () => {
  return <Button>Done</Button>;
};

export default SubmitButton;
