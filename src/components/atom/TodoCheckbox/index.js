import React from "react";
import styled from "@emotion/styled";

const CheckboxInput = styled.div`
  position: relative;
  width: 16px;
  height: 16px;
  border-radius: 16px;
  border: solid 1px #bdbdbd;
  background-color: ${(props) => (props.checked ? "#007aff" : "#fff")};
  margin: 2px 0;
`;

const Checkmark = styled.svg`
  width: 14px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  fill: none;
  stroke: #fff;
  stroke-width: 2;
  stroke-linecap: round;
  stroke-linejoin: round;
  stroke-dasharray: 20px;
  transition: stroke-dashoffset 0.2s ease-in-out;
`;

const TodoCheckbox = ({ checked, onChange }) => {
  const handleCheckboxClick = () => {
    onChange(!checked);
  };

  return (
    <CheckboxInput checked={checked} onClick={handleCheckboxClick}>
      {checked && (
        <Checkmark checked={checked} viewBox="0 0 24 24">
          <path d="M20,6L9,17l-5-5" />
        </Checkmark>
      )}
    </CheckboxInput>
  );
};

export default TodoCheckbox;
