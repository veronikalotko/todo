import React from "react";
import styled from "@emotion/styled";

const DateStyled = styled.div`
  font-size: 0.7rem;
  color: ${(props) => (props.past ? "#ff3d00" : "#757575")};
  display: flex;
  align-items: center;
  margin: 5px 0;
  gap: 5px;

  p {
    margin: 0;
  }

  path {
    fill: ${(props) => (props.past ? "#ff3d00" : "#757575")};
  }
`;

const TodoDate = ({ date }) => {
  const now = new Date();
  const isPast = new Date(date) < now;
  return (
    <DateStyled past={isPast}>
      <svg xmlns="http://www.w3.org/2000/svg" width="10" height="11">
        <path
          fill="#757575"
          fillRule="evenodd"
          d="M7.25 4.75a.5.5 0 1 0 0 1 .5.5 0 0 0 0-1ZM5 4.75a.5.5 0 1 0 0 1 .5.5 0 0 0 0-1Zm-2.25 0a.5.5 0 1 0 0 1 .5.5 0 0 0 0-1ZM7.25 7a.5.5 0 1 0 0 1 .5.5 0 0 0 0-1ZM5 7a.5.5 0 1 0 0 1 .5.5 0 0 0 0-1ZM2.75 7a.5.5 0 1 0 0 1 .5.5 0 0 0 0-1Zm6.5-3.75v5.5c0 .425-.325.75-.75.75h-7a.737.737 0 0 1-.75-.75v-5.5h8.5ZM3 0h-.75v1H0v7.75c0 .825.675 1.5 1.5 1.5h7c.825 0 1.5-.675 1.5-1.5V1H7.75V0H7v1H3V0Z"
        />
      </svg>
      <div type="date">{date}</div>
    </DateStyled>
  );
};

export default TodoDate;
